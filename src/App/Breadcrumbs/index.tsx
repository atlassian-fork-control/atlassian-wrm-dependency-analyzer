import React, { Component } from 'react';
import { withRouter, RouteComponentProps } from 'react-router';

import { BreadcrumbsStateless, BreadcrumbsItem } from '@atlaskit/breadcrumbs';
import { getEntryPointByIdx, getChunkByIdx } from '../../utils/data';

import './style.less';

/**
 * @router /entry/:entryPointIdx/resource/:resourceName
 * @router /entry/:entryPointIdx/chunk/:chunkIdx/resource/:resourceName
 */
interface IRouterParams {
    entryPointIdx: string;
    chunkIdx: string;
    resourceName: string;
}

interface IBreadcrumb {
    href: string;
    text: string;
}

interface IBreadcrumbsProps extends RouteComponentProps<IRouterParams> {
    content?: Array<IBreadcrumb>;
}

class Breadcrumbs extends Component<IBreadcrumbsProps> {
    render() {
        const items = this.getItems();

        if (!Array.isArray(items) || items.length === 0) {
            return null;
        }

        return (
            <div className="breadcrumbs">
                <BreadcrumbsStateless>
                    {items.map((item, key) => (
                        <BreadcrumbsItem key={key} href={item.href} text={item.text} />
                    ))}
                </BreadcrumbsStateless>
            </div>
        );
    }

    getItems() {
        const breadcrumbs: Array<IBreadcrumb> = [
            {
                href: '/',
                text: 'Home',
            },
        ];
        const { params } = this.props.match;
        const entryPointIdx = parseInt(params.entryPointIdx, 10);
        const chunkIdx = parseInt(params.chunkIdx, 10);

        if (entryPointIdx === undefined) {
            return breadcrumbs;
        }

        const entryPoint = getEntryPointByIdx(entryPointIdx);

        if (entryPoint) {
            breadcrumbs.push({
                href: `#/entry/${params.entryPointIdx}`,
                text: entryPoint.name,
            });
        }

        if (chunkIdx) {
            const chunk = getChunkByIdx(entryPointIdx, chunkIdx);

            breadcrumbs.push({
                href: `#entry/${params.entryPointIdx}/chunk/${chunkIdx}/resource/${params.resourceName}`,
                text: chunk.name,
            });
        }

        if (params.resourceName) {
            breadcrumbs.push({
                href: `#entry/${params.entryPointIdx}/resource/${params.resourceName}`,
                text: params.resourceName,
            });
        }

        return breadcrumbs;
    }
}

export default withRouter<IBreadcrumbsProps, any>(Breadcrumbs);
