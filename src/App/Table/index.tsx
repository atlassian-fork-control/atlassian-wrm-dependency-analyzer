import React, { Component, ReactElement } from 'react';
import sortBy from 'lodash/sortby';
import throttle from 'lodash/throttle';

import DynamicTable from '@atlaskit/dynamic-table';
import FieldTextStateless from '@atlaskit/field-text';

import './style.less';

enum SortOrder {
    ASC = 'ASC',
    DESC = 'DESC',
}

interface ITableHead {
    key: string;
    content: string | number | ReactElement;
    isSortable?: boolean;
}

interface ITableProps {
    label?: ReactElement | string;
    filterKey?: string;
    head?: Array<ITableHead>;
    rows: Array<Array<ITableRow>>;
}

interface ITableState {
    sortKey: string | null;
    sortOrder: SortOrder;
    filter: string;
}

interface ISortEvent {
    key: string;
    sortOrder: SortOrder;
}

export interface ITableRow<T = string> {
    key: T;
    content: string | number | JSX.Element | ReactElement;
    value?: string | number;
}

export default class Table extends Component<ITableProps, ITableState> {
    get tableHead() {
        const { head: headCells } = this.props;

        if (!headCells) {
            return {};
        }

        return {
            cells: headCells,
        };
    }

    get tableRows() {
        let { rows } = this.props;
        const { filterKey } = this.props;

        if (filterKey) {
            const { filter } = this.state;
            const filterRe = new RegExp(`${filter}`, 'gi');

            rows = rows.filter(cells => {
                const cellForFilter = cells.find(cell => cell.key === filterKey);

                if (!cellForFilter) {
                    return true;
                }

                if (cellForFilter.value && filterRe.test(cellForFilter.value.toString())) {
                    return true;
                }

                return false;
            });
        }

        const sortedRows = this.sort(rows);

        return sortedRows.map(cells => ({
            cells: cells,
        }));
    }

    constructor(props: ITableProps) {
        super(props);

        this._throttledSetState = throttle(this.setState, 700);
        this.state = {
            sortKey: null,
            sortOrder: SortOrder.ASC,
            filter: '',
        };
    }

    _throttledSetState: (state: any) => void;

    render() {
        const { label, rows } = this.props;
        const { sortKey, sortOrder } = this.state;

        if (!rows || rows.length === 0) {
            return null;
        }

        return (
            <div className="table">
                {label ? (
                    <div className="table__head">
                        <h2>{label}</h2>
                        {this.renderFilter()}
                    </div>
                ) : null}

                <DynamicTable
                    head={this.tableHead}
                    rows={this.tableRows}
                    defaultSortKey={sortKey}
                    defaultSortOrder={sortOrder}
                    onSort={this.handleSort}
                />
            </div>
        );
    }

    renderFilter() {
        const { filterKey } = this.props;

        if (!filterKey) {
            return null;
        }

        return (
            <div className="atomic-sizes__filter">
                <FieldTextStateless placeholder="Filter" onChange={this.handleChangeFilter} />
            </div>
        );
    }

    handleChangeFilter = (event: React.FormEvent<HTMLInputElement>) => {
        const { currentTarget } = event;

        this._throttledSetState({
            filter: currentTarget.value,
        });
    };

    handleSort = (e: ISortEvent) => {
        this.setState({
            sortKey: e.key,
            sortOrder: e.sortOrder,
        });
    };

    sort = (rows: Array<Array<ITableRow>>) => {
        const { sortKey, sortOrder } = this.state;

        const sortedDependencies = sortBy(rows, (cells: Array<ITableRow>) => {
            const cell = cells.find((c: any) => c.key === sortKey);

            if (!cell) {
                return -1;
            }

            return cell.value;
        });

        return sortOrder === SortOrder.DESC ? sortedDependencies : sortedDependencies.reverse();
    };
}
