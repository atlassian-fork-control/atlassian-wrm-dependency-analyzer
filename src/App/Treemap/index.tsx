import React, { Component } from 'react'
import { withSize } from 'react-sizeme';
import { withRouter } from 'react-router';

import * as d3 from 'd3';

const PALETTE = [
    '#77ACA2',
    '#3A445D',
    '#9DBEBB',
    '#468189',
    '#2A4494',
    '#224870',
    '#031926'
];

import './style.css';

export interface INodeDescriptor {
    name: string;
    value: number;
    children?: Array<INodeDescriptor>;
}

interface ITreemapProps {
    history: any,
    location: any,
    match: any,
    id: string;
    data: INodeDescriptor;
    size?: {
        width: number;
        height: number;
    },
    resourcePath?: Array<string | number>;
    heightRatio?: number;
}

interface ITreeNodeDescriptor extends d3.HierarchyRectangularNode<INodeDescriptor> {
    color: string;
    clipUid: string;
}

interface ITreemapState {
    tooltipVisible: boolean;
    tooltipPayload: ITreeNodeDescriptor | null;
    tooltipPosition: {
        x: number;
        y: number;
    };
}

const DEFAULT_PAYLOAD = {
    data: {
        name: ''
    },
    value: 0
};

class Treemap extends Component<ITreemapProps, ITreemapState> {
    state = {
        tooltipVisible: false,
        tooltipPayload: null,
        tooltipPosition: {
            x: 0,
            y: 0
        }
    }

    _container: HTMLDivElement | null = null;

    _getId = () => {
        const escapedId = this.props.id.replace(/\.|\+/g, '_');

        return `treemap_${escapedId}`;
    };
    
    _getWidth = () => this.props.size ? this.props.size.width : 1000;

    render() {
        const width = this._getWidth();
        const height = this.getContainerHeight(width);

        const {tooltipVisible, tooltipPosition, tooltipPayload = DEFAULT_PAYLOAD} = this.state;
        const styles = {
            transform: `translate(${tooltipPosition.x}px, ${tooltipPosition.y}px)`
        };

        return (
            <div
                className="tree-map"
                ref={this._refContainerCallback}
                onMouseOut={this._handleContainerMouseOut}
                onMouseMove={this._handleContainerMouseMove}
                >
                <svg
                    id={this._getId()}
                    className="treemap"
                    fill="transparent"
                    width={width}
                    height={height}
                    />
                <div className={`tree-map__node-tooltip ${tooltipVisible ? 'tree-map__node-tooltip_visible' : ''}`} style={styles}>
                    {tooltipPayload ? (
                       <>
                            <div>{tooltipPayload.data.name}</div>
                            <div>{(tooltipPayload.value / 1024).toFixed(1)}kb</div>
                        </>
                    ) : ''}
                </div>
            </div>
        );
    }

    showNodeTooltip(nodeData: ITreeNodeDescriptor) {
       this.setState({
           tooltipVisible: true,
           tooltipPayload: nodeData
       });
    }

    hideNodeTooltip() {
        this.setState({
            tooltipVisible: false,
            tooltipPayload: null
        });
    }

    _handleContainerMouseMove = (event: React.MouseEvent): void => {
        // TODO: throttle & calculate offset once

        if (!this._container) {
            return;
        }
        
        const {left: containerLeft, top: containerTop} = this._container.getBoundingClientRect();

        this.setState({
            tooltipPosition: {
                x: event.clientX - containerLeft,
                y: event.clientY - containerTop
            }
        });
    }

    _handleContainerMouseOut = () => {
        this.hideNodeTooltip();
    } 

    _handleDependencyMouseOver = (nodeData: ITreeNodeDescriptor) => {
        this.showNodeTooltip(nodeData);
    }

    _handleDependencyMouseClick = (node: ITreeNodeDescriptor) => {
        const { data: { name } } = node;
        const { history } = this.props;

        history.push(`/resource?id=${name}`);
    }

    _refContainerCallback = (ref: HTMLDivElement) => {
        this._container = ref;
    }

    componentDidMount() {
        const {data} = this.props;
        const id = this._getId();

        if (!data) {
            return;
        }

        this.renderDependenciesLayout(id, data);
    }

    getContainerHeight(width = this._getWidth()) {
        const {heightRatio = 0.4} = this.props;
        return width * heightRatio;
    }

    renderDependenciesLayout(containerId: string, data: INodeDescriptor) {
        const width = this._getWidth();
        const height = this.getContainerHeight(width);
        const root = d3.hierarchy(data)
            .sum(d => d.value);

        const treemapLayout = d3.treemap();
    
        treemapLayout
            .tile(d3.treemapBinary)
            .size([width, height])
            .paddingOuter(0)
            .paddingTop(30)
            .paddingInner(0)
            .round(true);
    
        const rootNode = treemapLayout(root) as ITreeNodeDescriptor;

        const nodes = d3.select(`#${containerId}`)
            .selectAll('g')
            .data(rootNode.descendants())
            .enter()
            .append('g')
            .on('click', this._handleDependencyMouseClick)
            .attr('transform', d => 'translate(' + [d.x0, d.y0] + ')');

        nodes
            .append('rect')
            .on('mousemove', this._handleDependencyMouseOver)
            .attr('width', function(d) { return d.x1 - d.x0; })
            .attr('height', function(d) { return d.y1 - d.y0; })
            .attr('fill', this.getNodeColor)
            .attr('fill-opacity', this.getNodeOpacity)
            .attr('class', 'tree-map__node');

            nodes
            .append("clipPath")
            .attr('id', (d, num) => {
                const id = `${d.data.name}_${num}`;
                d.clipUid = id;
                return id;
            })
            .append('rect')
            .attr('width', function(d) { return d.x1 - d.x0; })
            .attr('height', function(d) { return d.y1 - d.y0; });

        nodes
            .append('text')
            .attr('clip-path', d => `url(#${d.clipUid})`)
            .attr('dx', 6)
            .attr('dy', 20)
            .attr('class', 'tree-map__node-label')
            .text(d => {
                if (d.value && (d.value / 1024) < 30) {
                    return '';
                }
                return `${d.data.name} (${d.value ? (d.value / 1024).toFixed(1) : ''}kb)`;
            });
    }

    getNodeColor = (d: ITreeNodeDescriptor, idx: number) => {
        if (d.depth === 0) {
            return '#fff';
        }

        const color = PALETTE[idx % PALETTE.length];

        const backgroundColor = d.parent ? d.parent.color || color : color;

        d.color = backgroundColor;

        return backgroundColor;
    }

    getNodeOpacity = (d: ITreeNodeDescriptor) => {
        if (!d.value) {
            return 1;
        }

        if (d.parent && d.parent.value) {
            const value = d.value / 1024;
            const parentValue = d.parent.value / 1024;

            return Math.max((value / parentValue) + 0.2, 0.4);
        }

        return d.depth;
    }
}


export default withRouter<ITreemapProps, any>(withSize()(Treemap));