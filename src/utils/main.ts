import { IWebresourceDescriptor, IWebresourceSizes, WebresourceKey } from '../App/types';
import { getSizesReport } from './data';

export const getWebresourceSize = (webresource: IWebresourceDescriptor): IWebresourceSizes => {
    const css = webresource.css.grouped.reduce((acc, { size }) => acc + size, 0) / 1024;
    const js = webresource.js.grouped.reduce((acc, { size }) => acc + size, 0) / 1024;

    return {
        css,
        js,
        total: css + js,
    };
};

export interface IResourcesUsage {
    [key: string]: {
        count: number;
        dependencyFor: Array<WebresourceKey>;
    };
}

// TODO refactor that for ./data.js methods
export const getResourceUsageMap = (externalDependencies: Array<IWebresourceDescriptor>) => {
    return externalDependencies.reduce(
        (acc, webresource) => {
            const resources = webresource.js.resources;

            for (const resource of resources) {
                if (!acc[resource]) {
                    acc[resource] = {
                        count: 0,
                        dependencyFor: [],
                    };
                }

                acc[resource].count += 1;
                acc[resource].dependencyFor.push(webresource.webresourceKey);
            }

            return acc;
        },
        {} as IResourcesUsage
    );
};

export const getResourceUsageInfo = (resourcesUsageMap: IResourcesUsage, webresource: IWebresourceDescriptor) => {
    const sizesReport = getSizesReport();
    const dependencies = webresource.js.resources;
    const usage = {
        sharedCount: 0,
        sharedSize: 0,
        uniqCount: 0,
        uniqSize: 0,
    };

    for (const dependency of dependencies) {
        const resourceUsage = resourcesUsageMap[dependency];
        if (resourceUsage.count > 1) {
            usage.sharedSize += sizesReport.all[dependency].total;
            usage.sharedCount += 1;
        } else {
            usage.uniqSize += sizesReport.all[dependency].total;
            usage.uniqCount += 1;
        }
    }

    return usage;
};