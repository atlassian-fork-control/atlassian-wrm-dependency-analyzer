const path = require('path');
const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const DESTINATION_FOLDER = 'dist';
const WORKING_FOLDER = __dirname;

const commonConfig = {
    entry: {
        index: path.join(WORKING_FOLDER, 'src', 'index.tsx')
    },

    devtool: false,

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
    },

    output: {
        publicPath: path.join(WORKING_FOLDER, DESTINATION_FOLDER),
        path: path.join(WORKING_FOLDER, DESTINATION_FOLDER),
        filename: '[name].bundle.js',
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'awesome-typescript-loader',
                        options: {
                            silent: true,
                            useCache: true,
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader'],
            },
            {
                test: /\.(png|jpg|svg|gif|eot|ttf|woff)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                        },
                    },
                ],
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'image-webpack-loader',
                    },
                ],
            },
        ],
    },
};

const prodConfig = {
    mode: 'production',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
              cache: true,
              parallel: true,
              sourceMap: false,
            }),
          ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(WORKING_FOLDER, 'static', 'report.html'),
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackInlineSourcePlugin(),
    ],
};

const devConfig = {
    mode: 'development',
    optimization: {
        minimize: false
    },
    devtool: 'cheap-module-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(WORKING_FOLDER, 'static', 'prefilled-dev-report.html'),
        }),
    ]
};

const watchConfig = {
    output: {
        publicPath: ''
    },
    devServer: {
        index: './static/prefilled-dev-report.html',
        historyApiFallback: true,
        open: true,
        hot: true
    }
}

module.exports = env => { 
    if (env === 'production') {
        return merge(commonConfig, prodConfig);
    }

    if (env === 'watch') {
        return merge(commonConfig, devConfig, watchConfig);
    }

    return merge(commonConfig, devConfig);
}